# ecp-cookie-init
Utility for obtaining short-lived cookies for accessing Shibbolized SPs from
command-line tools (e.g., curl or git)

[![pipeline status](https://git.ligo.org/authpublic/ecp-cookie-init/badges/master/pipeline.svg)](https://git.ligo.org/authpublic/ecp-cookie-init/commits/master)
[![coverage](https://git.ligo.org/authpublic/ecp-cookie-init/badges/master/coverage.svg)](https://git.ligo.org/authpublic/ecp-cookie-init/commits/master)
